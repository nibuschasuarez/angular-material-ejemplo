import { Injectable } from '@angular/core';
import { UsuarioDataI } from '../interfaces/usuario.interface';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  [x: string]: any;

  
  listaUsuarios: UsuarioDataI[] = [
    {usuario: 'jperez', nombre: 'Juan', apellido: 'Perez', sexo: 'Masculino'}, 
    {usuario: 'mgomez', nombre: 'Maria', apellido: 'Gomez', sexo: 'Masculino'},
    {usuario: 'ngarcia', nombre: 'Nilda', apellido: 'Garcia', sexo: 'Femenino'},
    {usuario: 'kliop', nombre: 'Kevin', apellido: 'Liop', sexo: 'Masculino'},
    {usuario: 'hmarino', nombre: 'Hernan', apellido: 'Marino', sexo: 'Masculino'},
    {usuario: 'mmendizabal', nombre: 'Magdiel', apellido: 'Mendizabal', sexo: 'Femenino'}
    
  ];

  constructor() { }

  getUsuario():UsuarioDataI[] {
    //slice retorna una copia del array
    return this.listaUsuarios.slice();
  }


  agregarUsuario(usuario:UsuarioDataI):void{ 
    this.listaUsuarios.unshift(usuario);

  }

  eliminarUsuario(usuario:string){
  
  this.listaUsuarios = this.listaUsuarios.filter(data => {
    return data.usuario !== usuario;
  });
}

buscarUsuario(id: string): UsuarioDataI{
  //o retorna un json {} vacio
  return this.listaUsuarios.find(element=> element.usuario === id) || {} as UsuarioDataI;
} 
modificarUsuario(user: UsuarioDataI){
  this.eliminarUsuario(user.usuario);
  this.agregarUsuario(user);
}


}
