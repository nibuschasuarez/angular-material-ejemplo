import { Component, OnInit } from '@angular/core'; 
import { MenuService } from 'src/app/services/menu.service';
import { Menu } from 'src/app/interfaces/menu';



@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit { 

  menu: Menu[] = [];

  constructor(private _menuSErvice: MenuService) { }

  ngOnInit(): void {
    this.cargaMenu();
  }

  cargaMenu(): void{
    this._menuSErvice.getMenu().subscribe(data => {
      console.log(data);
      this.menu = data;
      
    })
  }

}
