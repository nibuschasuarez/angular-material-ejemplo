import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { usuarioI } from 'src/app/interfaces/usuario.interface';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form!:FormGroup;
  loading: boolean = false;

  constructor(private fb: FormBuilder,
    private _snackbar:MatSnackBar,
    private router:Router) { 
    this.formulario();
   }

  ngOnInit(): void {
  }

  formulario():void{
    this.form=this.fb.group({
      usuario:['',Validators.required],
      password:['', Validators.required]
    })
  }

  Ingresar():void{
    console.log(this.form.value);
    const Usuario:usuarioI = {
      usuario:this.form.value.usuario,
      password:this.form.value.password
    }

    console.log(Usuario);

    if(Usuario.usuario ==='jPerez' &&  Usuario.password==='admin123'){
      //redireccionamos al dashboard
      this.fakeLoading();
    }else{
      //Mostramos un mensaje de error
      this.error();
      this.form.reset();
    }
    
  }

  fakeLoading():void{
    this.loading = true;

    //Rediccionamos al dashboard
    setTimeout(()=>{
      this.loading= false;
      this.router.navigate(['dashboard'])
    }, 1500);
  }

  error():void {
    this._snackbar.open('usuario o contraseña incorrecto', '' ,{
      duration:5000,
      horizontalPosition:'center',
      verticalPosition:'bottom',
    })
  }
}

// snackbar primer parametro mensaje, segundo boton o accion
